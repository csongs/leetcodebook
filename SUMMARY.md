# Summary

* [Introduction](README.md)



## Array
- [1.Two Sum](/algorithms/1.two-sum.md)

 

## Math
- [1175. Prime Arrangements](/algorithms/1175.-prime-arrangements.md)

## String
- [14. longest common prefix](/algorithms/14.-longest-common-prefix.md)
